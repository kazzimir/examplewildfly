package com.kazimir.bortnik.webstart.servlets.repository;

import com.kazimir.bortnik.webstart.servlets.model.Users;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class EntityManagerDAO {
    @PersistenceContext(unitName = "myUnit")
    EntityManager entityManager;


    public List<Users> findAll() {
        String query = "from " + Users.class.getName() + " c";
        Query q = entityManager.createQuery(query);
        return q.getResultList();
    }
}
