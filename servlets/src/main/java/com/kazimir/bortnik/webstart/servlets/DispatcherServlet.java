package com.kazimir.bortnik.webstart.servlets;

import com.kazimir.bortnik.webstart.servlets.model.Users;
import com.kazimir.bortnik.webstart.servlets.repository.EntityManagerDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class DispatcherServlet extends HttpServlet {
    @EJB
    private EntityManagerDAO entityManagerDAO;


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Users> users = entityManagerDAO.findAll();
        req.setAttribute("users", users);
        req.getRequestDispatcher("/WEB-INF/pages/users.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
