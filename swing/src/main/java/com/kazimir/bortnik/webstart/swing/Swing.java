package com.kazimir.bortnik.webstart.swing;

import javax.swing.*;

public class Swing {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Webstart Application Example");
        final JLabel label = new JLabel("Hello World");

        label.setText("Swing Webstart App");

        frame.getContentPane().add(label);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
